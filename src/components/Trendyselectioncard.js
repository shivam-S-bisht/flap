import React from 'react';
import {Image, Text, View, StyleSheet, TouchableOpacity} from 'react-native';


export default class Trendyselectioncard extends React.Component{


    state = {
        toscreen: "Bookdescription"
    }

    componentDidMount () {
        // this.props.props.props.navigation.setOptions({
        //     setListenClicked: this.props.props.setListenClicked
        // })
    }

    render() {
        
        // console.log(this.props)
        const {product_id, product_name , author, duration, thumbnail_url} = this.props.trendycard;

        return(
            <View>
                <TouchableOpacity
                    style={styles.toptouchable}
                    onPress={() => {
                        // console.log(this.props.props)
                        this.props.props.props.navigation.push('Splash', {to: this.state.toscreen, from: 'Tabbars', product_id: parseInt(product_id)})
                    }}
                >

                    {/* hardcode */}
                    {thumbnail_url? 
                    <Image source={{uri: thumbnail_url}} style={{borderTopLeftRadius: 5, borderBottomLeftRadius: 5}} />
                    :
                    <Image source={require("../../assets/trendypic.png")} style={{borderTopLeftRadius: 5, borderBottomLeftRadius: 5}} />
                    }
                    {/* hardcode */}

                    <View style={styles.contentviewable}>
                        <Text style={{fontSize: 16, fontWeight: 'bold', color: '#fff', marginBottom: 10}}>{product_name.length <= 17 ? product_name:`${product_name.slice(0, 17)} ...`}</Text>
                        <Text style={{color: '#DCFFA8', fontSize: 15, marginBottom: 10}}>{author}</Text>

                        {/* hardcode */}
                        <Text style={{color: '#FFF', fontSize: 15}}>Total Read {duration ? duration : 14}: Min</Text>
                        {/* hardcode */}
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles=StyleSheet.create({
    toptouchable: {
        flex: 1,
        backgroundColor: '#463229',
        marginRight: 15,
        borderRadius: 5,
        flexDirection: 'row'
    },

    contentviewable: {
        padding: 10,
        paddingTop: 20,
        flex: 1
    }
})