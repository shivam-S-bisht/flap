import React from 'react';
import { View, Text, StyleSheet, FlatList, ScrollView, SafeAreaView, TextInput, TouchableOpacity, LogBox } from 'react-native';

import Exploretabflapbookscard from "../components/Exploretabflapbookscard";


export default class Viewall extends React.Component {
    render() {
        // console.log(this.props.route.params.data.length)
        return (
            <View>

                <Text style={styles.texttitle}>{this.props.route.params.title}</Text>
                <FlatList
                    keyExtractor={(_, index) => index.toString()}
                    data={this.props.route.params.data}
                    renderItem={({ item }) => <Exploretabflapbookscard explorecard={item} props={this.props} />}
                    showsVerticalScrollIndicator={false}
                />

            </View>
        )
    }
}

const styles = StyleSheet.create({
    texttitle: {
        fontSize: 20,
        fontWeight: 'bold',
        textAlign: 'center',
        paddingVertical: 10
    }
})