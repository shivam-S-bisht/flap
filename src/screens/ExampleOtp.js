import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import OTPInputView from '@twotalltotems/react-native-otp-input'

export default class App extends Component {
    render() {
        return (
            <View
                style={{}}
            >
                <OTPInputView
                    pinCount={6}
                    autoFocusOnLoad={false}
                    codeInputFieldStyle={{
                        borderRadius: 5,
                        borderColor: '#BFC4D4',
                        borderWidth: 2,
                        marginBottom: 20,
                        fontSize: 18,
                        color: '#626779',
                        textAlign: 'center',
                        fontWeight: 'bold',
                    }}
                    onCodeFilled = {(otp => {
                        this.setState({otp})
                    })}
                    
                />
            </View>
        );
    }
}


