// PRE-PROCESSESS
import React from 'react';
import { Text, View, Image, Animated } from 'react-native';
import axios from 'react-native-axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { StackActions } from '@react-navigation/native';

import bookdescription from '../infos/bookdescription';

// import { StackActions } from 'react-navigation';

export default class Splash extends React.Component {

    constructor() {
        super();
        this.state = {
            animatedValue: new Animated.Value(0)
        }

        this._isMounted = false;

    }


    // FIRST
    // called before mounting
    componentDidMount() {
        // console.log(this.props.route)
        this.animatedpromise()
    }



    // FIRST splash
    async splashfunc() {

        const { found, token } = await this.gettoken()

        if (found) {

            // check the validity of token .......
            this.validatetoken(token).then(res => {

                if (res == 200) {

                    this.dummiedata().then(data => {
                        // console.log(data)
                        // this.props.navigation.replace('Tabbars', { data, tokenvalid: true })
                        // this.props.navigation.reset(this.props.navigation.navigate("Tabbars", { data, tokenvalid: true }))
                        this.props.navigation.reset({
                            index: 0,
                            routes: [{
                                name: 'Tabbars',
                                params: {
                                    data,
                                    tokenvalid: true
                                }
                            }],

                        })
                    })

                }
                else {
                    this.props.navigation.replace("LoginSignupchoose")
                }
            })

            //


        } else {
            // this.props.navigation.replace('Onboarding')
            // this.props.navigation.reset(this.props.navigation.replace("Onboarding"))
            // this.props.navigation.reset({
            //     index: 0,
            //     routes: [{ name: 'Onboarding' }],
            // });
            this.props.navigation.reset({
                index: 0,
                routes: [{
                    name: 'Onboarding',
                }],

            })

        }

    }

    



    // tabbar props handler ...
    async tabbarfunc(productidparam) {


        switch (this.props.route.params.to) {
            case "Bookdescription":

                const { found, token } = await this.gettoken()
                this.validatetoken(token).then(resp => {

                    this.getproductdetails(productidparam).then(productdetails => {
                        this.getproductchapters(productidparam).then(productchapters => {
                            this.getproducttags(productidparam).then(async producttags => {

                                const pdt = Object.values(producttags) //object -> list
                                var tagdetailslist = []

                                const bgcolor = ['#EEE5C9', '#BFD2E6', '#D3EEC9', '#EEE5C9']
                                let promises = pdt.map((Object) => {
                                    return new Promise(async resolve => {
                                        return this.gettagdetails(Object.tag_name)
                                            .then((res) => {
                                                var array = res.hits.hits.map((item, index) => {
                                                    return { product_id: parseInt(item._id), product_name: item._source.product_name, author: item._source.author, duration: item._source.duration, thumbnail_url: item._source.thumbnail_url, background: bgcolor[index] }
                                                })
                                                tagdetailslist.push(...array)
                                                resolve("ok")
                                            }).catch(e => console.log(e))
                                    })
                                })

                                await Promise.all(promises)
                                // console.log("PRODUCT CHAPTERS:", productchapters)
                                this.props.navigation.replace("Bookdescription", { productdetails, productchapters, tagdetailslist, resp, token })


                            })
                        })

                    })
                }).catch(_ => this.props.navigation.replace("LoginSignupchoose"))


                break;

            case "Tagscreen":
                this.getallfavs().then(allfav => {

                    this.gettagdetails(this.props.route.params.tagname).then(data => {
                        this.props.navigation.replace("Tagscreen", { data, tagname: this.props.route.params.tagname, allfav })
                    })
                })

                break;

            case "Login":
                // console.log("Im inside login case in slpash+")
                this.props.navigation.replace("LoginSignupchoose")
                break;

        }


    }








    // createnewaccount props handler ...
    async createnewaccountfunc(isEmail) {


        axios.post('/signup', {

            emailMobile: this.props.route.params.emailorphone,
            password: this.props.route.params.password

        }).then((res) => {

            if (res.status == 200) {
                if (isEmail) {   // if email then login again because no socket implementation
                    this.props.navigation.replace("Login")

                } else {
                    this.props.navigation.push("Enterotp", { mobile: this.props.route.params.emailorphone })
                }


            } else {
                this.props.navigation.goBack()
            }

        }).catch((e) => {

            this.props.navigation.goBack()

        })


    }



    // login props handler ...
    async loginfunc() {

        const res = this.loginapifunc(this.props.route.params.emailorphone, this.props.route.params.password)
        res.then(async (token) => {
            await this.puttoken(token)
            await this.savecred(this.props.route.params.emailorphone)
            this.dummiedata().then(data => {
                this.props.navigation.reset({
                    index: 0,
                    routes: [{
                        name: 'Tabbars',
                        params: {
                            data,
                            tokenvalid: true
                        }
                    }],

                })

            })
        }).catch(err => {
            this.props.navigation.goBack()
            console.log(`ERROR: ${err}`)
        })
    }



    async onboardingfunc() {

        this.dummiedata().then(data => {

            this.props.navigation.reset({
                index: 0,
                routes: [{
                    name: 'Tabbars',
                    params: {
                        data
                    }
                }],

            })
        })

    }




    async getduration() {

        var duration = bookdescription.duration;

        var min = Math.floor(duration / 60);
        var sec = Math.floor(duration % 60);

        if (`${min}`.length == 1) {
            min = `0${min}`
        }

        if (`${sec}`.length == 1) {
            sec = `0${sec}`
        }

        return { duration: `${min}:${sec}`, maxvalue: duration }
    }




    async bookdescriptionfunc(product_id, chapter_id, totalch, product_name, author, thumbnail_url) {

        const to = this.props.route.params.to
        if (to == 'Pdfview') {
            const { found, token } = await this.gettoken()
            if (found) {
                this.validatetoken(token).then(status => {
                    if (status == 200) {
                        this.getproductfiles(product_id, token).then(data => this.props.navigation.replace('Pdfview', { data, currch: chapter_id, totalch, tokenvalid: true, product_name, author, thumbnail_url }))
                    } else {
                        if (chapter_id == 1) {
                            this.getproductfiles(product_id, token).then(data => this.props.navigation.replace('Pdfview', { data, currch: chapter_id, totalch, tokenvalid: false, product_name, author, thumbnail_url }))
                        } else {
                            this.props.navigation.replace("LoginSignupchoose")
                        }
                    }
                }).catch()
            } else {
                if (chapter_id == 1) {
                    this.getproductfiles(product_id, token).then(data => this.props.navigation.replace('Pdfview', { data, currch: chapter_id, totalch, tokenvalid: false, product_name, author, thumbnail_url }))
                } else {
                    this.props.navigation.replace("LoginSignupchoose")
                }
            }

        } else {
            this.props.navigation.replace(to)
        }

    }



    // FIRST
    // splash animation
    animatedpromise() {
        const animatedpromise = new Promise(async res => {
            Animated.timing(this.state.animatedValue, {
                toValue: 210,
                duration: 1000,
                useNativeDriver: false
            }).start();

            await this.timer()
            res()
        })

        animatedpromise.then(() => {
            try {
                switch (this.props.route.params.from) {
                    // case 'Tabbars': this.tabbarfunc(this.props.route.params.product_id); break;
                    case 'Tabbars': this.tabbarfunc(2); break;
                    case 'Createnewaccount': this.createnewaccountfunc(this.props.route.params.isEmail); break;
                    case 'Login': this.loginfunc(); break;
                    // case 'Bookdescription': this.bookdescriptionfunc(this.props.route.params.product_id, this.props.route.params.chapter_id, this.props.route.params.totalch); break;
                    case 'Bookdescription': this.bookdescriptionfunc(2, this.props.route.params.chapter_id, this.props.route.params.totalch, this.props.route.params.product_name, this.props.route.params.author, this.props.route.params.thumbnail_url); break;
                    case "Onboarding": this.onboardingfunc(); break;
                    case "Profile": this.logoutfunc(); break;
                    case "Settings": this.settingsfunc(); break;
                    case "Enterotp": this.verifysignupfunc(); break;
                    case "Forgotpassword": this.forgetpassapifunc(); break;
                    case "VerifyAndResetPassword": this.forgetpassfunc(); break;
                    case "Loginviaotp": this.loginviaotp(this.props.route.params.emailMobile); break;
                    case "LoginEnterOtp": this.verifylogin(this.props.route.params.emailorphone, this.props.route.params.otp); break;
                }
            } catch (e) {
                this.splashfunc()
            }
        })
    }



    // timeout for splash animation
    timer() {
        return new Promise(res => {
            setTimeout(() => {
                res()
            }, 1000)
        })
    }



    // read token 
    async gettoken() {

        const token = await AsyncStorage.getItem('@token')
        try {
            if (token != null) {
                return { found: true, to: this.props.route.params.to, token: token }
            } else {
                return { found: false, to: 'LoginSignupchoose', token }
            }
        } catch {
            return { found: true, to: 'Onboarding', token }
        }
    }



    // save token 
    async puttoken(val) {
        try {
            await AsyncStorage.setItem('@token', val)

        } catch (e) {
            console.log(e)
        }
    }



    // save credentials 
    async savecred(val) {
        try {
            await AsyncStorage.setItem('@emailVal', val)
        } catch (e) {
            console.log(e)
        }
    }



    // API CALLS -> login
    async loginapifunc(emailMobile, password) {

        return new Promise((resolve, reject) => {
            axios.post('/login', {
                emailMobile,
                password
            }).then((res) => {
                if (res.status == 200) {
                    resolve(res.data.token)
                } else {
                    reject('No account found')
                }
            }).catch(() => {
                reject('response from server:400, Some error occured')
            })
        })
    }





    // API CALLS -> create new account
    async createnewaccountapifunc(emailMobile, password) {

        return new Promise((resolve, reject) => {
            axios.post('/signup', {
                emailMobile,
                password
            }).then((res) => {
                if (res.status == 200) {
                    console.log(res.data)
                    resolve('OTP sent')
                } else {
                    reject('Account already exists')
                }
            }).catch(() => {
                reject('response from server:400, Some error occured')
            })

        })
    }






    async verifysignupfunc() {

        this.verifysignupapifunc(this.props.route.params.emailorphone, this.props.route.params.otp)
            .then(async (token) => {
                await this.puttoken(token)
                await this.savecred(this.props.route.params.emailorphone)
                this.dummiedata().then(data => {
                    this.props.navigation.reset({
                        index: 0,
                        routes: [{
                            name: 'Tabbars',
                            params: {
                                data,
                                tokenvalid: true
                            }
                        }],

                    })
                })
            }).catch(err => {
                this.props.navigation.goBack()
                console.log(`ERROR: ${err}`)
            })
    }


    // API CALLS -> verify create new account
    async verifysignupapifunc(emailMobile, otp) {

        return new Promise((resolve, reject) => {
            axios.post('/verifySignup', {
                emailMobile,
                otp
            }).then((res) => {
                if (res.status == 200) {
                    resolve(res.data.token)
                } else {
                    reject('Something went wrong')
                }
            }).catch(() => {
                reject('response from server:400, Some error occured')
            })
        })
    }



    // API CALLS -> resend otp
    async resendotpapifunc(emailMobile, userId) {

        return new Promise((resolve, reject) => {
            axios.post('/resendotp', {
                emailMobile,
                userId
            }).then((res) => {
                if (res.status == 200) {
                    resolve('OTP resent successfully')
                } else {
                    reject('User not present, Signup to continue')
                }
            }).catch(() => {
                reject('response from server:400, Some error occured')
            })

        })
    }



    // API CALLS -> forget pass
    async forgetpassapifunc() {

        axios.post('/forgetPassword', {
            emailMobile: this.props.route.params.emailphone
        }).then((res) => {
            if (res.status == 200) {
                this.props.navigation.replace("VerifyAndResetPassword", { data: res.data })
            } else {
                console.log("forgetpassapifunc::", res.status)
                this.props.navigation.goBack()
            }
        }).catch((err) => {
            console.log('forgetpassapifunc::', err)
            this.props.navigation.goBack()
        })

    }



    // API CALLS -> verify forget pass
    async verifyforgetpassapifunc(emailMobile, userId, otp, password) {

        return new Promise((resolve, reject) => {
            axios.post('/verifyForgetPassword', {
                emailMobile,
                userId,
                otp,
                password
            }).then((res) => {
                if (res.status == 200) {
                    resolve(res.data.token)
                } else {
                    reject('Account does not exists')
                }
            }).catch((err) => {
                reject('response from server:400, Some error occured')
            })

        })
    }




    async forgetpassfunc() {

        const res = this.verifyforgetpassapifunc(this.props.route.params.emailMobile, this.props.route.params.userId, this.props.route.params.otp, this.props.route.params.password)
        res.then(async (token) => {
            await this.puttoken(token)
            await this.savecred(this.props.route.params.emailorphone)
            this.dummiedata().then(data => {
                this.props.navigation.replace('Tabbars', { data, tokenvalid: true })
            })
        }).catch(err => {
            this.props.navigation.goBack()
            console.log(`ERROR: ${err}`)
        })
    }



    async settingsfunc() {

        AsyncStorage.getItem('@token').then(token => {
            axios.get(`/flapmore-user/profile`, {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            }
            ).then((res) => {

                if (res.status == 200) {
                    this.props.navigation.replace("Profile", { data: res.data })
                } else {
                    this.props.navigation.replace("LoginSignupchoose")
                }

            }).catch(() => this.props.navigation.replace("LoginSignupchoose"))
        }).catch(() => {
            this.props.navigation.replace("LoginSignupchoose")
        })

    }



    async getallfavs() {
        return AsyncStorage.getItem("@lib").then(async lib => {
            lib = JSON.parse(lib)
            if (lib && "fav" in lib) {
                return lib.fav
            } else {
                return []
            }
        }
        ).catch(e => console.log("Tagscreen Error:------------->>>>>>>>>>", e))
    }



    // API CALLS -> dynamic dummie data using tags
    async dummiedata() {
        const pdt = ["Fiction"] //object -> list
        var tagdetailslist = []

        const bgcolor = ['#EEE5C9', '#BFD2E6', '#D3EEC9', '#EEE5C9']
        let promises = pdt.map((Object) => {
            return new Promise(async resolve => {
                return this.gettagdetails(Object.tag_name)
                    .then((res) => {

                        var array = res.hits.hits.map((item, index) => {
                            return { product_id: parseInt(item._id), product_name: item._source.product_name, author: item._source.author, duration: item._source.duration, thumbnail_url: item._source.thumbnail_url, background: bgcolor[index] }
                        })
                        tagdetailslist.push(...array)
                        resolve("ok")
                    }).catch(e => console.log(e))
            })
        })

        await Promise.all(promises)
        tagdetailslist = tagdetailslist
        console.log(tagdetailslist.length)
        return tagdetailslist

    }



    // API CALLS -> product details
    async productdetailsapifunc(product_id) {
        const token = await AsyncStorage.getItem('@token')

        await axios.get('/flapmore/product', {
            headers: {
                'Authorization': `Bearer ${token}`
            },
            params: {
                product_id
            }
        }

        ).then((res) => {
        }).catch(e => console.log(e))
    }



    // API CALLS -> product all tags 
    async producttagsapifunc(product_id) {
        const token = await AsyncStorage.getItem('@token')

        await axios.get('/flapmore/product/tags', {
            headers: {
                'Authorization': `Bearer ${token}`
            },
            params: {
                product_id
            }
        }

        ).then((res) => {
            console.log(res, '\n', JSON.stringify(res.data))
        }).catch(e => console.log(e))
    }




    // API CALLS -> list all categories 
    async getallcategoriesapifunc() {
        const token = await AsyncStorage.getItem('@token')

        await axios.get('/flapmore/categories', {
            headers: {
                'Authorization': `Bearer ${token}`
            }
        }

        ).then((res) => {
            console.log(res, '\n', JSON.stringify(res.data))
        }).catch(e => console.log(e))
    }





    async loginviaotp(emailMobile) {
        console.log("++++++++++++++++++", emailMobile)
        axios.post('/loginotp', {
            emailMobile
        }).then((res) => {
            if (res.status == 200) {
                console.log('OTP sent successfully')
                this.props.navigation.replace("LoginEnterOtp", { mobile: emailMobile })
            } else {
                console.log('Bad credentials')
                this.props.navigation.goBack()
            }
        }).catch((_) => {
            console.log('response from server:400, Some error occured')
            this.props.navigation.goBack()
        })
    }



    async verifylogin(emailMobile, otp) {
        console.log(emailMobile, otp)
        axios.post('/verifyloginotp', {
            emailMobile,
            otp
        }).then(async (res) => {
            if (res.status == 200) {
                console.log("OTP verified")
                await this.puttoken(res.data.token)
                await this.savecred(this.props.route.params.emailorphone)
                this.dummiedata().then(data => {
                    this.props.navigation.reset({
                        index: 0,
                        routes: [{
                            name: 'Tabbars',
                            params: {
                                data,
                                tokenvalid: true
                            }
                        }],

                    })
                })

            } else {
                console.log('Something went wrong')
                this.props.navigation.goBack()
            }
        }).catch(() => {
            console.log('response from server:400, Some error occured')
            this.props.navigation.goBack()
        })
    }




    async validatetoken(token) {

        return await axios.get(`/flapmore-user/profile`, {
            headers: {
                'Authorization': `Bearer ${token}`
            }
        }

        ).then((res) => {
            return res.status
        }).catch(e => console.log(e))
    }




    // API CALLS -> get tag details 
    async gettagdetails(tagname) {
        const token = await AsyncStorage.getItem('@token')




        return await axios.get(`/flapmore/search?category_id=1&tags=${tagname}`, {
            headers: {
                'Authorization': `Bearer ${token}`
            }
        }

        ).then((res) => {
            return res.data
        }).catch(e => console.log(e))
    }



    // API CALLS -> get product details 
    async getproductdetails(productid) {
        const token = await AsyncStorage.getItem('@token')

        return await axios.get(`/flapmore/product?product_id=${productid}`, {
            headers: {
                'Authorization': `Bearer ${token}`
            }
        }

        ).then((res) => {
            return Object.values(res.data)[0]
        }).catch(e => console.log(e))
    }








    // API CALLS -> get product files
    async getproductchapters(productid) {
        const token = await AsyncStorage.getItem('@token')

        return await axios.get(`/flapmore/product/chapters?product_id=${productid}`
        ).then((res) => {
            return res.data
        }).catch(_ => null)
    }



    async getproductfiles(productid, token) {
        // const token = await AsyncStorage.getItem('@token')

        return await axios.get(`/flapmore/product/files?product_id=${productid}`, {
            headers: {
                'Authorization': `Bearer ${token}`
            }
        }

        ).then((res) => {
            return res.data
        })
    }






    // API CALLS -> get product related tags
    async getproducttags(productid) {

        return await axios.get(`/flapmore/product/tags?product_id=${productid}`).then((res) => {

            if (res.status == 200) {
                console.log(res.data)
                return res.data

            } else {
                const tags = {
                    "1": {
                        "tag_name": "Fiction",
                        "tag_id": 1
                    },
                    "2": {
                        "tag_name": "History",
                        "tag_id": 2
                    }
                }
                return tags
            }

        }).catch(_ => this.props.navigation.replace('LoginSignupchoose'))


    }



    async clearalldata() {
        return AsyncStorage.getAllKeys()
            .then(keys => AsyncStorage.multiRemove(keys))
            .then(() => console.log('success'));
    }


    logoutfunc() {

        switch (this.props.route.params.to) {
            case "Tabbars":
                this.clearalldata().then(() => {

                    this.dummiedata().then(data => {
                        this.props.navigation.reset({
                            index: 0,
                            routes: [{
                                name: 'Tabbars',
                                params: {
                                    data,
                                    tokenvalid: false
                                }
                            }],

                        })
                    })

                })
                break;

        }
    }






    // MAIN -------->>>>
    render() {

        // this.searchwithcategory()
        return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: 'white' }}>
                <Image source={require('../../assets/splash-icon.png')} style={{ marginBottom: 5 }} />
                <Image source={require('../../assets/home-iconname.png')} style={{ marginBottom: 5 }} />
                <Text style={{ color: '#696C7B' }}>Flap more for smart contents</Text>

                <View style={{ backgroundColor: '#E3E3E5', width: 210, height: 6, marginTop: 10, borderRadius: 10 }}>
                    <Animated.View style={{ backgroundColor: '#3D6DFF', height: 6, width: this.state.animatedValue, borderRadius: 10 }}>

                    </Animated.View>
                </View>
            </View>
        );
    }
}