import React from 'react';
import { StyleSheet, Dimensions, View, StatusBar, TouchableOpacity, Text, ScrollView, FlatList } from 'react-native';
import Slider from '@react-native-community/slider';

import Pdf from 'react-native-pdf';
import Icon from 'react-native-vector-icons/Ionicons';

import HTML from "react-native-render-html";
import { setlistenClicked } from '../infos/listenClicked';


class Pdfview extends React.Component {

  constructor(props) {
    super(props)
    this.handleViewableItemsChanged = this.handleViewableItemsChanged.bind(this)
    this.viewabilityConfig = { viewAreaCoveragePercentThreshold: 100 }
    this.playbooksurilist = Object.values(this.props.route.params.data).map(item=> item.audio_url)
  }



  state = {
    lastpage: 1,
    currpage: this.props.route.params.currch,
    totalpages: this.props.route.params.totalch,
    render: false,
    playbookuri: null,
  }


  handleViewableItemsChanged({ viewableItems }) {
    if (viewableItems.length)
      this.setState({ currpage: viewableItems[0].index + 1, playbookuri: viewableItems[0].item.audio_url})
  
  }

  render() {

    const chcontexts = Object.values(this.props.route.params.data)
    return (
      <View style={styles.topviewable}>

        <View style={styles.firstviewable}>
          <TouchableOpacity
            onPress={() => this.props.navigation.goBack()}
          >
            <Icon name="chevron-back-sharp" size={30} color="#151522" />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {

              setlistenClicked(true)
              // console.log(this.state.listen)

              this.props.navigation.navigate('Tabbars', {
                from: 'Bookdescription',
                to: 'Musicplayer',
                title: this.props.product_name ? this.props.product_name : 'Nil',
                author: this.props.author ? this.props.author : 'Nil',
                thumbnail_url: this.props.thumbnail_url,
                // hardcoded
                playbookuri:
                  this.state.playbookuri ?
                    this.state.playbookuri
                    :
                    'https://www.soundhelix.com/examples/mp3/SoundHelix-Song-8.mp3',
                // hardcoded

                currindex: this.state.currpage-1,
                playbookurilist: this.playbooksurilist

              })
            }}
          >
            <Icon name="headset-outline" size={30} color="#151522" />
          </TouchableOpacity>
        </View>

        <View style={{ flex: 1 }}>
          <FlatList
            showsHorizontalScrollIndicator={false}
            keyExtractor={(item, index) => index.toString()}
            data={chcontexts}
            horizontal={true}
            decelerationRate={'fast'}
            snapToInterval={Dimensions.get('window').width}
            initialScrollIndex={this.props.route.params.currch - 1}
            viewabilityConfig={this.viewabilityConfig}
            onViewableItemsChanged={this.handleViewableItemsChanged}

            renderItem={(item, index) => {
              return (
                <ScrollView
                  style={{
                    flex: 1,
                    width: Dimensions.get('window').width,
                    padding: Dimensions.get('window').width / 9,
                    paddingLeft: Dimensions.get('window').width / 9
                  }}
                  showsVerticalScrollIndicator={true}
                >
                  <HTML source={{ html: `<h1>${item.item.chapter_heading}</h1><br /><h2>${item.item.chapter_text}` }} tagsStyles={{ h1: { margin: 0, padding: 0 }, h2: { margin: 0, padding: 0 } }}
                  />
                </ScrollView>
              )

            }}
          />
        </View>

        <View style={styles.sliderviewable}>
          <View style={{ alignItems: 'center', justifyContent: 'center', marginVertical: 5 }}><Text style={styles.currpagetext}>{this.state.currpage}</Text></View>
          <Slider
            style={{ width: Dimensions.get('window').width }}
            minimumValue={1}
            maximumValue={this.state.totalpages}
            minimumTrackTintColor="#3D6DFF"
            maximumTrackTintColor="#3D6DFF"
            value={this.state.currpage}
            onValueChange={(value) => this.setState({ currpage: value })}
            step={1}
            thumbTintColor={'#3D6DFF'}

          />
        </View>
      </View>

    )

  }
}

const styles = StyleSheet.create({
  topviewable: {
    flex: 1,
    // justifyContent: 'flex-start',
    // alignItems: 'center',
    paddingTop: StatusBar.currentHeight,
    backgroundColor: '#fff'
  },

  firstviewable: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingHorizontal: 20
  },

  pdf: {
    flex: 1,
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },

  currpagetext: {
    textAlign: 'center',
    fontSize: 18,
    fontWeight: 'bold',
    backgroundColor: '#3D6DFF',
    paddingHorizontal: 8,
    borderRadius: 8,
    paddingVertical: 3,
    color: '#fff'
  },

  sliderviewable: {
    marginVertical: 10
  }
});

export default Pdfview;